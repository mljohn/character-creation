/**
 * Author: Michelle John
 */
package main;

import main.gui_elements.CharacterCreationFrame;
import main.gui_elements.CharacterCreationPanel;
import main.gui_elements.CharacterCreationTextArea;

/**
 * Main entry into the application.
 */
public class CharacterCreation {

  /**
   * Main entry into the application.
   *
   * @param args arguments used to start the application
   */
  public static void main (String[] args) {
    var program = new CharacterCreation();
    program.startGui();
  }

  /**
   * Initializes and displays the GUI.
   */
  private void startGui() {
    var infoPanel = new CharacterCreationPanel();

    var nameLabel = new CharacterCreationTextArea(false, false, 1, 1, 40, 17);
    nameLabel.setText("Name: ");
    var name = new CharacterCreationTextArea(true, true, 42, 1, 100, 17);
    infoPanel.add(nameLabel);
    infoPanel.add(name);

    var raceLabel = new CharacterCreationTextArea(false, false, 143, 1, 35, 17);
    raceLabel.setText("Race: ");
    var race = new CharacterCreationTextArea(true, true, 179, 1, 100, 17);
    infoPanel.add(raceLabel);
    infoPanel.add(race);

    var levelLabel = new CharacterCreationTextArea(false, false, 280, 1, 35, 17);
    levelLabel.setText("Level: ");
    var level = new CharacterCreationTextArea(true, true, 316, 1, 25, 17);
    infoPanel.add(levelLabel);
    infoPanel.add(level);

    var sizeLabel = new CharacterCreationTextArea(false, false, 342, 1, 30, 17);
    sizeLabel.setText("Size: ");
    var size = new CharacterCreationTextArea(true, true, 373, 1, 25, 17);
    infoPanel.add(sizeLabel);
    infoPanel.add(size);

    var classLabel = new CharacterCreationTextArea(false, false, 1, 19, 40, 17);
    classLabel.setText("Class: ");
    var characterClass = new CharacterCreationTextArea(true, true, 42, 19, 100, 17);
    infoPanel.add(classLabel);
    infoPanel.add(characterClass);

    var genderLabel = new CharacterCreationTextArea(false, false, 143, 19, 46, 17);
    genderLabel.setText("Gender: ");
    var gender = new CharacterCreationTextArea(true, true, 190, 19, 40, 17);
    infoPanel.add(genderLabel);
    infoPanel.add(gender);

    var alignmentLabel = new CharacterCreationTextArea(false, false, 231, 19, 60, 17);
    alignmentLabel.setText("Alignment: ");
    var alignment = new CharacterCreationTextArea(true, true, 292, 19, 25, 17);
    infoPanel.add(alignmentLabel);
    infoPanel.add(alignment);

    var ageLabel = new CharacterCreationTextArea(false, false, 318, 19, 29, 17);
    ageLabel.setText("Age: ");
    var age = new CharacterCreationTextArea(true, true, 348, 19, 25, 17);
    infoPanel.add(ageLabel);
    infoPanel.add(age);

    var heightLabel = new CharacterCreationTextArea(false, false, 1, 37, 40, 17);
    heightLabel.setText("Height: ");
    var height = new CharacterCreationTextArea(true, true, 42, 37, 30, 17);
    infoPanel.add(heightLabel);
    infoPanel.add(height);

    var weightLabel = new CharacterCreationTextArea(false, false, 73, 37, 42, 17);
    weightLabel.setText("Weight: ");
    var weight = new CharacterCreationTextArea(true, true, 116, 37, 30, 17);
    infoPanel.add(weightLabel);
    infoPanel.add(weight);

    var hairLabel = new CharacterCreationTextArea(false, false, 147, 37, 30, 17);
    hairLabel.setText("Hair: ");
    var hair = new CharacterCreationTextArea(true, true, 178, 37, 45, 17);
    infoPanel.add(hairLabel);
    infoPanel.add(hair);

    var eyesLabel = new CharacterCreationTextArea(false, false, 224, 37, 30, 17);
    eyesLabel.setText("Eyes: ");
    var eyes = new CharacterCreationTextArea(true, true, 255, 37, 45, 17);
    infoPanel.add(eyesLabel);
    infoPanel.add(eyes);

    var deityLabel = new CharacterCreationTextArea(false, false, 1, 55, 35, 17);
    deityLabel.setText("Deity: ");
    var deity = new CharacterCreationTextArea(true, true, 37, 55, 150, 17);
    infoPanel.add(deityLabel);
    infoPanel.add(deity);

    var homelandLabel = new CharacterCreationTextArea(false, false, 188, 55, 60, 17);
    homelandLabel.setText("Homeland: ");
    var homeland = new CharacterCreationTextArea(true, true, 249, 55, 150, 17);
    infoPanel.add(homelandLabel);
    infoPanel.add(homeland);

    var frame = new CharacterCreationFrame("Character Creation", 100, 100, 450, 450);
    frame.add(infoPanel);
    frame.display();
  }
}
