/**
 * Author: Michelle John
 */
package main.gui_elements;

import javax.swing.*;
import javax.swing.border.LineBorder;

import static java.awt.Color.BLACK;
import static java.awt.Color.WHITE;

/**
 * Creates a {@link JTextArea} to use in this application.
 */
public class CharacterCreationTextArea extends JTextArea {

  /**
   * Constructor.
   *
   * @param editable if the text field should be editable
   * @param hasBorder if the text area should have a border
   * @param x the x-coord of the upper left corner of the element
   * @param y the y-coord of the upper left corner of the element
   * @param width the width of the element
   * @param height the height of the element
   */
  public CharacterCreationTextArea(boolean editable, boolean hasBorder, int x, int y, int width, int height) {
    super();
    setEditable(editable);
    setBackground(WHITE);
    setForeground(BLACK);
    if (hasBorder) setBorder(new LineBorder(BLACK));
    setBounds(x, y, width, height);
  }
}
