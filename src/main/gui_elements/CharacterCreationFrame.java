/**
 * Author: Michelle John
 */
package main.gui_elements;

import javax.swing.*;

/**
 * Creates a {@link JFrame} to use in the application.
 */
public class CharacterCreationFrame extends JFrame {

  /**
   * Constructor.
   *
   * @param title the title of the frame
   * @param x the x-coord to set the frame
   * @param y the y-coord to set the frame
   * @param width the width of the frame
   * @param height the height of the frame
   */
  public CharacterCreationFrame(String title, int x, int y, int width, int height) {
    super(title);
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    setBounds(x, y, width, height);
  }

  /**
   * Sets the visibility of the {@link JFrame} to true.
   */
  public void display() {
    setVisible(true);
  }
}
