/**
 * Author: Michelle John
 */
package main.gui_elements;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

/**
 * Creates a {@link JPanel} to use in the application.
 */
public class CharacterCreationPanel extends JPanel {

  /**
   * Constructor.
   */
  public CharacterCreationPanel() {
    super();
    setBorder(new EmptyBorder(5, 5, 5, 5));
    setLayout(null);
  }
}
